import re

import secrets
import string

def checkpassword(password):
    # Vérifier la longueur minimale
    if len(password) < 10:
        return False
    
    # Vérifier s'il contient au moins 2 chiffres
    if len(re.findall(r'\d', password)) < 2:
        return False
    
    # Vérifier s'il contient au moins un caractère spécial non-alphanumérique
    if not re.search(r'[^a-zA-Z0-9]', password):
        return False
    
    # Chaînes de caractères à exclure/invalides
    exclure = [ "azer", "Azer", "AzEr", "aZeR", "AZER", "qwer", "QWER", "123", "321"]
    if any(exclure in password for exclure in exclure):
        return False
    
    # Vérifier l'absence de deux caractères identiques consécutifs
    for i in range(len(password) - 1):
        if password[i] == password[i+1]:
            return False
        
    # Vérifier si le mot de passe est un palindrome
    if password == password[::-1]:
        return False
    
    # Toutes les conditions sont satisfaites
    return True

def generatepassword(length):
    caracteres_possibles = string.ascii_letters + string.digits + string.punctuation
    passwords = []
    
    while len(passwords) < length:
        password_length = secrets.randbelow(11) + 10  # Génère un nombre aléatoire entre 10 et 20
        password = ''.join(secrets.choice(caracteres_possibles) for _ in range(password_length))
        if checkpassword(password):
            passwords.append(password)
        else:
            with open("mots_de_passe_rejetes.txt", "a") as file:
                file.write(password + "\n")
    return passwords


def main():
    
    chosen_element = 0

    print("#############################################################################")
    print("########                                                             ########")
    print("########               My Company Super Client Number 1'             ########")
    print("######## ----------------------------------------------------------- ########")
    print("######## Customer relationship management for Super Client Number 1  ########")
    print("######## ----------------------------------------------------------- ########")
    print("########                                                             ########")
    print("########                    Choose a option:                         ########")
    print("########                                                             ########")
    print("########        1) Generate a password  |  2) Analyze a password     ########")
    print("########                             3) Exit                         ########")
    print("########                                                             ########")
    print("########                                                             ########")
    print("#############################################################################")
    chosen_element = input("Enter a number from 1 to 3: ")
    
    if int(chosen_element) == 1:
        print("Generating a password")
        return 1
    elif int(chosen_element) == 2:
        print("Analyzing a password")
        return 2
    elif int(chosen_element) == 3:
        print("Exiting program")
        return 3        
    else:
        print("Sorry, the value entered must be a number from 1 to 3, then try again!")

if __name__ == "__main__":



    while True:
        result = main()
        if result == 1 :
            length = input("Choose a number between 1 and 20: ")
            while not length.isdigit() or int(length) > 20 or int(length) < 1:
                length = input("Please choose a valid integer between 1 and 20: ")
            
            passwords = generatepassword(int(length))
            for password in passwords:
                print(password)
        

        if result == 2 :
            password = input("What is your password : ")
            if checkpassword(password):
                print("Valid password")
            else:
                print("Invalid password")
        if result == 3 :
            break
        
                
